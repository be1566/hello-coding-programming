﻿using System;

namespace MorePractice4_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int coin = 10;
            int aram, uram;
            aram = coin / 2 - 1;
            coin = coin - aram;
            uram = coin / 2 + 2;
            coin = coin - uram;
            Console.Write("주머니에 남은 동전의 개수 : ");
            Console.WriteLine(coin);
            Console.Write("아람이가 가져간 동전의 개수 : ");
            Console.WriteLine(aram);
            Console.Write("우람이가 가져간 동전의 개수 : ");
            Console.WriteLine(uram);
        }
    }
}
