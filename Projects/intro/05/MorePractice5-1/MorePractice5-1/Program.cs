﻿using System;

namespace MorePractice5_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("첫 번째 실수를 입력하세요: ");
            double number1 = double.Parse(Console.ReadLine());
            Console.Write("두 번째 실수를 입력하세요: ");
            double number2 = double.Parse(Console.ReadLine());

            Console.Write(number1 + " + " + number2 + " = ");
            Console.WriteLine(number1 + number2);

            Console.Write(number1 + " - " + number2 + " = ");
            Console.WriteLine(number1 - number2);
            
            Console.Write(number1 + " * " + number2 + " = ");
            Console.WriteLine(number1 * number2);

            Console.Write(number1 + " / " + number2 + " = ");
            Console.WriteLine(number1 / number2);
        }
    }
}
