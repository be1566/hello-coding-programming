﻿using System;

namespace MorePractice5_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("주머니에는 동전이 10개 들어 있습니다.");
            Console.WriteLine("어머니는 몇 개의 동전을 주머니에 넣었나요?");
            int coinInPoket = 10;
            coinInPoket += int.Parse(Console.ReadLine());

            Console.WriteLine("아람이는 몇 개의 동전을 꺼냈나요?");
            int aram = int.Parse(Console.ReadLine());
            coinInPoket -= aram;

            Console.WriteLine("우람이는 몇 개의 동전을 꺼냈나요?");
            int uram = int.Parse(Console.ReadLine());
            coinInPoket -= uram;

            Console.WriteLine("주머니에 남아 있는 동전의 개수는 " + coinInPoket + "개입니다.");
        }
    }
}
