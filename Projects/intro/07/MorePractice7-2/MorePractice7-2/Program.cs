﻿using System;

namespace MorePractice7_2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] scores = new double[5];

            Console.Write("국어 점수를 입력하세요: ");
            scores[0] = double.Parse(Console.ReadLine());

            Console.Write("영어 점수를 입력하세요: ");
            scores[1] = double.Parse(Console.ReadLine());

            Console.Write("수학 점수를 입력하세요: ");
            scores[2] = double.Parse(Console.ReadLine());

            Console.Write("과학 점수를 입력하세요: ");
            scores[3] = double.Parse(Console.ReadLine());

            Console.Write("사회 점수를 입력하세요: ");
            scores[4] = double.Parse(Console.ReadLine());

            double sum = 0, avg;
            for(int i = 0; i < scores.Length; i++)
            {
                sum += scores[i];
            }

            avg = sum / scores.Length;

            Console.Write("총점: ");
            Console.WriteLine(sum);
            Console.Write("평균: ");
            Console.WriteLine(avg);
        }
    }
}
