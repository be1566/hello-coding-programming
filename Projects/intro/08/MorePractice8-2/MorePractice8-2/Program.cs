﻿using System;

namespace MorePractice8_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("총 학생수를 입력하세요: ");
            int totalStudent = int.Parse(Console.ReadLine());

            int index = 0;
            int[] scores = new int[3];
            int[] sum = new int[totalStudent];
            int[] avg = new int[totalStudent];

            while (index < totalStudent)
            {
                Console.Write(index);
                Console.WriteLine("번째 학생의 점수를 입력받습니다.");

                Console.Write("국어: ");
                scores[0] = int.Parse(Console.ReadLine());
                sum[index] += scores[0];

                Console.Write("수학: ");
                scores[1] = int.Parse(Console.ReadLine());
                sum[index] += scores[1];

                Console.Write("영어: ");
                scores[2] = int.Parse(Console.ReadLine());
                sum[index] += scores[2];

                avg[index] = sum[index] / 3;

                index++;
            }

            Console.WriteLine();
            Console.WriteLine("각 학생의 총점과 평균을 출력합니다.");
            index = 0;
            while(index < totalStudent)
            {
                Console.Write(index);
                Console.Write("번째 학생의 총점: ");
                Console.WriteLine(sum[index]);

                Console.Write(index);
                Console.Write("번째 학생의 평균: ");
                Console.WriteLine(avg[index]);

                Console.WriteLine();

                index++;
            }
        }
    }
}
