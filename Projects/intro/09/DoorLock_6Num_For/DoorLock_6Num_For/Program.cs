﻿using System;

namespace DoorLock_6Num_For
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] passcodeNumbers = { 6, 2, 1, 9, 4, 7 };

            int passcodeLength = 6;
            int[] userInput = new int[passcodeLength];

            // 심화문제 9-1
            int count = 0;

            while (count < 5)
            {
                for(int passcodeIndex = 0; passcodeIndex < passcodeLength; passcodeIndex++)
                {
                    Console.Write(passcodeIndex);
                    Console.WriteLine("번째 숫자를 넣어주세요.");
                    userInput[passcodeIndex] = int.Parse(Console.ReadLine());
                }

                bool isPasswordCorrect = true;
                for(int passcodeIndex = 0; passcodeIndex < passcodeLength; passcodeIndex++)
                {
                    if(userInput[passcodeIndex] != passcodeNumbers[passcodeIndex])
                    {
                        isPasswordCorrect = false;
                        count++;
                        if(count == 5)
                        {
                            Console.WriteLine("비밀번호가 5번 틀렸습니다. 프로그램을 종료합니다.");
                            break;
                        }
                        else
                        {
                            Console.WriteLine("비밀번호가 틀렸습니다.");
                            break;
                        }
                    }
                }

                if (isPasswordCorrect)
                {
                    Console.WriteLine("문이 열렸습니다.");
                    break;
                }
            }
        }
    }
}
