﻿using System;

namespace MorePractice9_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] scores = new int[10];
            int sum = 0;
            int[] avg = new int[5];

            for(int i = 0; i < 5; i++)
            {
                Console.Write(i);
                Console.WriteLine("반");
                for(int j = 0; j < 10; j++)
                {
                    Console.Write(j);
                    Console.Write("번째 학생의 점수를 입력하세요: ");
                    scores[j] = int.Parse(Console.ReadLine());
                    sum += scores[j];
                }
                avg[i] = sum / 10;
                sum = 0;
            }

            Console.WriteLine();
            Console.WriteLine("각 반의 평균점수를 출력합니다.");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(i);
                Console.Write("반: ");
                Console.WriteLine(avg[i]);
            }
        }
    }
}
