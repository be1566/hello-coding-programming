﻿using System;

namespace MorePractice10_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            int number = random.Next(1, 101);

            while(true)
            {
                Console.WriteLine("숫자를 추측해 보세요(1 이상 100 이하의 숫자)");
                int guess = int.Parse(Console.ReadLine());

                if(guess > number)
                {
                    Console.WriteLine("더 작은 숫자입니다.");
                }
                else if(guess < number)
                {
                    Console.WriteLine("더 큰 숫자입니다.");
                }
                else if(guess == number)
                {
                    Console.WriteLine("정답입니다.");
                    break;
                }
            }
        }
    }
}
