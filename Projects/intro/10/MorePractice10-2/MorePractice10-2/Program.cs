﻿using System;

namespace MorePractice10_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            
            while(true)
            {
                int opponent = random.Next(1, 4);

                Console.WriteLine("하나를 고르세요(1: 가위, 2: 바위, 3: 보)");
                int user = int.Parse(Console.ReadLine());

                if(opponent == user)
                {
                    Console.WriteLine("비겼습니다.");
                }
                else if((opponent == 1 && user == 3) || (opponent == 2 && user == 1) || (opponent == 3 && user == 2))
                {
                    Console.WriteLine("졌습니다.");
                    break;
                }
                else if((opponent == 1 && user == 2) || (opponent == 2 && user == 3) || (opponent == 3 && user == 1))
                {
                    Console.WriteLine("이겼습니다.");
                    break;
                }
            }
        }
    }
}
